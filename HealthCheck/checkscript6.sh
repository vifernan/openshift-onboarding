#!/bin/bash 
mkdir healthchekdir
cd healthchekdir
oc new-project healthcheck
oc patch namespace healthcheck --type=merge -p '{"metadata": {"annotations": { "scheduler.alpha.kubernetes.io/defaultTolerations": "[{\"operator\": \"Exists\"}]"}}}'
oc get clusterversion version -o yaml |grep -e " clusterID:"; oc get clusterversion > clusterversion.txt
oc get secrets kubeadmin -n kube-system > kubeadmin.txt
oc get oauth/cluster -o json | jq '. |.spec.identityProviders' > oauth.txt
oc get nodes -o name > nomenodes.txt
oc get nodes > nodes.txt
oc get nodes -l node-role.kubernetes.io/master= -o name > nodemaster.txt
oc get nodes -l node-role.kubernetes.io/worker= -o name > nodeworker.txt
oc get nodes -l node-role.kubernetes.io/infra= -o name > nodeinfra.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.conditions'|grep reason ; done > nodeconditions.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.nodeInfo'|grep kubeProxyVersion ; done > nodekubeproxyversion.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.nodeInfo'|grep kernelVersion ; done > nodekernelversion.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.nodeInfo'|grep containerRuntimeVersion ; done > nodecontainerruntimeversion.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.nodeInfo'|grep kubeletVersion ; done > nodekubeletversion.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.nodeInfo'|grep osImage ; done > nodeosimage.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host crictl ps --name openvswitch ; done > nodeopenswitch.txt
oc adm top nodes > nodetop.txt
oc get csr > csr.txt
for i in $(cat nodemaster.txt) ; do echo $i && oc describe $i |grep OS|tr -d '[[:space:]]' && echo ''; done > osreleasemaster.txt 
for i in $(cat nodemaster.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep cpu ; done > cpumaster.txt
for i in $(cat nodemaster.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep memory ; done > memorymaster.txt
for i in $(cat nodemaster.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep ephemeral-storage ; done > estoragemaster.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  registry.redhat.io -ksI ; done > masterregistryio.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  quay.io -ksI ; done > masterquayio.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  sso.redhat.com -ksI ; done > masterredhatcom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  mirror.openshift.com -ksI ; done > mastermirroropencom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  api.openshift.com -ksI ; done > masterapiopencom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  storage.googleapis.com/openshift-release -ksI ; done > masterstoragegoolgeapicom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  cert-api.access.redhat.com -ksI ; done > mastercertapiaccessredhatcom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  api.access.redhat.com -ksI ; done > masterapiaccessredhatcom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  infogw.api.openshift.com -ksI ; done > masterinfogwapiopenshiftcom.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host curl  cloud.redhat.com -ksI ; done > mastercloudredhatcom.txt
for i in $(cat nodeinfra.txt) ; do echo $i && oc describe $i |grep OS|tr -d '[[:space:]]' && echo ''; done > osreleaseinfra.txt 
for i in $(cat nodeworker.txt) ; do echo $i && oc describe $i |grep OS|tr -d '[[:space:]]' && echo ''; done > osreleaseworker.txt 
for i in $(cat nodeinfra.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep cpu ; done > cpuinfra.txt
for i in $(cat nodeworker.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep cpu ; done > cpuworker.txt
for i in $(cat nodeinfra.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep memory ; done > memoryinfra.txt
for i in $(cat nodeworker.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep memory ; done > memoryworker.txt
for i in $(cat nodeinfra.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep ephemeral-storage ; done > estorageinfra.txt
for i in $(cat nodeworker.txt) ; do echo $i && oc get $i -o json | jq '. | .status.capacity'| grep ephemeral-storage ; done > estorageworker.txt
oc get nodes -o wide > nodeswide.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host cat /etc/selinux/config  |grep -v '#' |grep SELINUX=enforcing ; done > nodesselinux.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host systemctl show NetworkManager --property=ActiveState ; done > nodeactivenetworkmanager.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host systemctl show NetworkManager --property=SubState ; done > nodesubstatenetworkmanager.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host systemctl show kubelet --property=ActiveState ; done > nodeactivekubelet.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host systemctl show kubelet --property=SubState ; done > nodesubstatekubelet.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host systemctl show crio --property=ActiveState ; done > nodeactivecrio.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host systemctl show crio --property=SubState ; done > nodesubstatecrio.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host sudo /bin/top -b -n 5 -d 5 | grep Tasks -A20 ; done > nodetoptasks.txt
oc get proxy cluster -o yaml > clusterproxy.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host sudo chronyc sources ; done > nodetimedatectl.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host sudo chronyc tracking ; done > nodechronytracking.txt
oc get crd > crd.txt
oc get clusteroperators > clusteroperators.txt
for project in $(oc get projects |awk '{print $1}'|grep -v NAME);do  if [[ $(oc get subs -n $project) =~ "NAME" ]]; then echo -e "***************************************************************";echo -e "Namespace: " $project ""; oc get subs -n $project;echo -e "***************************************************************";fi;done > opersubs.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc get $i -o json | jq '. | .status.allocatable.pods' ; done > nodepods.txt
oc get pods -o wide -n openshift-monitoring > monitoringpods.txt
oc describe deployment prometheus-operator -n openshift-monitoring > deploymentprometheusoperator.txt
oc describe deployment grafana -n openshift-monitoring > deploymentgrafana.txt
oc describe daemonset ovs -n openshift-sdn > ovsdaemonset.txt
oc describe daemonset sdn -n openshift-sdn > sdndaemonset.txt
oc get pods -o wide -n openshift-sdn > sdnpods.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc describe $i  && echo ''; done > describenodes.txt
oc describe deployment.apps/etcd-quorum-guard -n openshift-etcd > describedeploymentetcdquorum.txt
oc get etcd -o=jsonpath='{range .items[0].status.conditions[?(@.type=="EtcdMembersAvailable")]}{.message}{"\n"}' > etcdmember.txt
oc get pods -n openshift-etcd |grep etcd > etcdpods.txt
for pod in $(oc get pods -n openshift-etcd -l app=etcd -o name); do echo ${pod}; oc rsh -n openshift-etcd ${pod}  bash -c 'etcdctl endpoint status -w table';echo -e "****************************\n";done > etcdstatus.txt
for pod in $(oc get pods -n openshift-etcd -l app=etcd -o name); do echo ${pod}; oc rsh -n openshift-etcd ${pod}  bash -c 'etcdctl endpoint health -w table';echo -e "****************************\n";done > etcdhealth.txt
for pod in $(oc get pods -n openshift-etcd -l app=etcd -o name); do echo ${pod}; oc rsh -n openshift-etcd ${pod}  bash -c 'etcdctl alarm list';echo -e "****************************\n";done > etcdalarmlist.txt
oc get openshiftapiserver -o=jsonpath='{range .items[0].status.conditions[?(@.type=="Encrypted")]}{.reason}{"\n"}{.message}{"\n"}' > openapicondition.txt
oc get deployment.apps/router-default -n openshift-ingress > routerdefault.txt
oc get pods -n openshift-ingress -o wide >> routerdefault.txt
oc get machines -n openshift-machine-api -o wide > machines.txt
oc get pods -n openshift-machine-api -o wide > machinepods.txt
oc get kubeapiserver -o=jsonpath='{range .items[0].status.conditions[?(@.type=="NodeInstallerProgressing")]}{.reason}{"\n"}{.message}{"\n"}' > kubeapicondition.txt
oc get kubecontrollermanager -o=jsonpath='{range .items[0].status.conditions[?(@.type=="NodeInstallerProgressing")]}{.reason}{"\n"}{.message}{"\n"}' > kubecontrollercondition.txt
oc get kubescheduler -o=jsonpath='{range .items[0].status.conditions[?(@.type=="NodeInstallerProgressing")]}{.reason}{"\n"}{.message}{"\n"}' > kubeschedulercondition.txt
oc get etcd -o=jsonpath='{range .items[0].status.conditions[?(@.type=="NodeInstallerProgressing")]}{.reason}{"\n"}{.message}{"\n"}' > etcdcondition.txt
oc get poddisruptionbudget --all-namespaces > poddisrupt.txt
oc get clusterresourceoverride cluster -n clusterresourceoverride-operator -o yaml > clusterresources.yaml
oc get mcp > mcp.txt
oc describe mcp master > mcpmaster.txt
oc describe mcp infra > mcpinfra.txt
oc describe mcp worker > mcpworker.txt
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host cat /proc/sys/kernel/random/entropy_avail ; done > nodeentroypy.txt 
for i in $(cat nomenodes.txt) ; do echo $i && oc debug $i -n healthcheck -- chroot /host df -hT /var ; done > nodevar.txt 
oc get namespaces | grep -v NAME | wc -l > totalnamespace.txt
oc get pods -A | awk '{ print $1 }' | uniq -c | sort -rn | head -10 > totalpodsdesc.txt
oc get services -A | grep -v NAME | wc -l > totalservices.txt
oc get pods -A | grep -v NAME | wc -l > totalpods.txt
oc get deployment -A | awk '{ print $1 }' | uniq -c | sort -rn | head -10 > totaldeploymentdesc.txt
for i in $(oc get pod -A  | grep -v Running | grep -v Completed |grep -v NAME |awk  '{print "Proyecto:"$1"||||""Pod:"$2"||||""status:"$4"||||restarts:"$5"||||age:"$6}') ; do echo $i ; echo -e "\r\n******************************************************************************************************\r\n" ; done > podsstatustotal.txt
oc get storageclass > storageclass.txt
oc get pods -n openshift-logging > loggingpods.txt
oc get pods -n openshift-logging -o name |grep elasticsearch-cdm | head -n 1 > elasticpodone.txt && sed -i 's|pod/||g' elasticpodone.txt
oc get pods -n openshift-logging -o name |grep elasticsearch-cdm > elasticpod.txt
for i in $(cat elasticpod.txt) ; do echo $i && oc exec -n openshift-logging -c elasticsearch $i -- health ; done > healthpodslogging.txt 
oc exec -n openshift-logging -c elasticsearch $(cat elasticpodone.txt) -- es_util --query=_cat/nodes?v > loggingnodesquey.txt
oc exec -n openshift-logging -c elasticsearch $(cat elasticpodone.txt) -- es_util --query=_cat/indices?v > loggingindices.txt
oc patch OCSInitialization ocsinit -n openshift-storage --type json --patch  '[{ "op": "replace", "path": "/spec/enableCephTools", "value": true }]'
sleep 20
oc get pods -n openshift-storage > odfpods.txt
oc get pods -n openshift-storage -o name |grep tools | head -n 1 > odftoolspod.txt && sed -i 's|pod/||g' odftoolspod.txt
oc exec -n openshift-storage $(cat odftoolspod.txt) -- ceph status > odfcephstatus.txt
oc exec -n openshift-storage $(cat odftoolspod.txt) -- ceph health detail > odfcephhealth.txt
oc exec -n openshift-storage $(cat odftoolspod.txt) -- ceph df detail > odfcephdf.txt
oc exec -n openshift-storage $(cat odftoolspod.txt) -- ceph osd status > odfcephosdstatus.txt
oc exec -n openshift-storage $(cat odftoolspod.txt) -- ceph osd tree > odfcephosdtree.txt
oc exec -n openshift-storage $(cat odftoolspod.txt) -- ceph osd df > odfcephosddf.txt
oc get pods -n open-cluster-management > acmpods.txt
oc get clustermanagers > acmclustermanagers.txt
oc get managedclusters > acmmanagedclusters.txt
oc get managedclusteraddons -A > acmmanagedclusteraddons.txt
oc delete project healthcheck
cd ..
tar cfz healthchekdir.tgz healthchekdir
rm -rf healthchekdir
oc get pods --sort-by='.status.containerStatuses[0].restartCount' -A